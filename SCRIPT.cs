﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class inventory : MonoBehaviour {

    private Item[,] inventorySlots = new Item[4, 3];

	// Use this for initialization
	void Start () 
    {
	    
	}
	
	// Update is called once per frame
	void Update () 
    {
	      
	}

    /// <summary>
    /// Adds an item to the inventory
    /// </summary>
    /// <param name="newItem">The newly acquired item that is to be added into the inventory</param>
    private void AddItem(Item newItem)
    {
        //for every row in the inventory
        for (int row = 0; row < 3; row++)
        {      
            //for every column in the current row
            for (int col = 0; col < 4; col++)
            {
                //if the new item is a potion check if it is able to stack with any other potions already in the inventory
                if (newItem.healthPotion == true || newItem.manaPotion == true)
                {
                    CheckForStack(newItem, inventorySlots[row, col]);
                }

                //if the current slot has no item in it
                if (inventorySlots[row, col] == null)
                {
                    //set the new item to be in this empty slot and break out of this process
                    inventorySlots[row, col] = newItem;
                    break;
                }
            }
        }
    }

    /// <summary>
    /// Switches the positions of 2 items in the inventory
    /// </summary>
    /// <param name="item1">The item the user moves into another location</param>
    /// <param name="item2">The item that is blocking the location of the first item's relocation</param>
    private void SwitchItem(Item item1, Item item2)
    {
        //creates a temporary item to hold the value of the second item
        Item tempHolder = item2;

        //assigns the second item to switch to the first item, and assigns the first item to now switch to the second item (1 swap)
        item2 = item1;
        item1 = tempHolder;

    }

    /// <summary>
    /// Deletes an item from the inventory
    /// </summary>
    /// <param name="item">the item which the user wants to remove</param>
    private void RemoveItem(Item item)
    {
        item = null;
    }

    private void CheckForStack(Item newItem, Item currentItem)
    {

    }

    private void RemoveGaps()
    {
        //for every row in the inventory
        for (int row = 3; row < 0; row++)
        {
            //for every column in the current row
            for (int col = 4; col < 0; col++)
            {
                if (inventorySlots[row, col] != null)
                {
                    if (col != 0)
                    {
                        if (inventorySlots[row, col - 1] == null)
                        {
                            SwitchItem(inventorySlots[row, col - 1], inventorySlots[row, col]);
                        }
                    }
                    else
                    {
                        if (inventorySlots[row - 1, 4] == null)
                        {
                            SwitchItem(inventorySlots[row - 1, 4], inventorySlots[row, col]);
                        }
                    }
                }
            }
        }
    }


}
